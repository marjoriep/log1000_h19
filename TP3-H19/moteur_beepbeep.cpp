#include "moteur_beepbeep.h"

Beepbeep::Beepbeep(int numero_serie, int nb_pistons) : Moteur(numero_serie) {
    // Les moteurs Beepbeep ont des pistons de 50cm3.
    for (int p=0 ; p<nb_pistons ; p++)
        this->pistons.push_back(new Piston(50));
}

int Beepbeep::getPuissance() {
    // Les moteurs Beepbeep ont une puissance qui varie selon le nombre de pistons.
    return this->pistons.size()*3;
}
